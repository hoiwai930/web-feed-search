(ns web-feed-search.handler
  (:require [compojure.api.sweet :refer :all]
            [bing-search.search :refer :all]
            [clojure.string :as str]
            [clj-http.client :as client]
            [ring.middleware.cors :refer [wrap-cors]]
            [net.cgrand.enlive-html :as html]
            [ring.util.http-response :refer :all]
            [schema.core :as s]))

(set-key! "l7mRdCWZ4gZVrV54z3W4dI6wc1iepMC5obu/NijibH8")

(def cache (atom {}))

(defn fetch-url [url]
  (html/html-resource (:body (client/get url {:as :stream :insecure? true}))))

(defn lookup[url]
  (println (html/select (fetch-url url) [[:link (html/attr-starts :type "application/")]]))
  (:href (:attrs (first (html/select (fetch-url url) [[:link (html/attr-starts :type "application/")]])))))

(defn distill [l url]
  (if (str/starts-with? l "http")
    l
    (if (str/starts-with? l "//")
      (str "http:" l)
      (str url l))))

(defn url [q]
  (let [r (search :Web q {:$format "json"})]
    (-> r :result first (get "Url"))))

(defn rss [q]
  (->
    (client/get "https://cloud.feedly.com/v3/search/feeds"
                {:query-params {:query q}
                 :as :json})
    :body :results first :feedId (subs 5)))


(defn url2 [q]
  (let [r (search :Web q {:$format "json"})]
    (map #(get % "Url") (-> r :result )) ))

(defn- end-with-slash [l]
  (filter #(re-matches #"http.*/" %) l))

(defn lookup2 [url]
  (try
    (do
      (:href (:attrs (first (html/select (fetch-url url) [[:link (html/attr-starts :type "application/")]])))))
    (catch Exception e nil)))

(defn grep2 [q]
  (if-let [c (get-in @cache q)]
    c
    (loop [[a b & more] (end-with-slash (url2 q))]
      (let [feed-url (distill (lookup2 a) a)]
        (if-not feed-url
          (recur more)
          (do 
            (swap! cache assoc-in q feed-url)
            feed-url))))))

(defn grep [x]
  (if-let [c (get-in @cache x)]
    c
    (do
      (let [link (-> x url)
            f (distill (lookup link) link)]
        (swap! cache assoc-in x f)
        f)
    )))

(def app
  (api
    {:swagger
     {:ui "/"
      :spec "/swagger.json"
      :data {:info {:title "Web-feed-search"
                    :description "Compojure Api example"}
             :tags [{:name "api", :description "some apis"}]}}}

    (context "/api" []
      :tags ["api"]

      (GET "/find" []
        :return {:result s/Str}
        :query-params [x :- s/Str]
        :summary "search rss with feedly"
        (ok {:result (rss x)}))

      (GET "/url" []
        :return {:result s/Str}
        :query-params [x :- s/Str]
        :summary "find feed"
        (ok {:result (first (url2 x))}))

      )))

(def handler
  (wrap-cors app :access-control-allow-origin [#".*"]
             :access-control-allow-methods [:get :put :post :delete]))
