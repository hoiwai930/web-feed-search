(defproject web-feed-search "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-time "0.11.0"] ; required due to bug in `lein-ring uberwar`
                 [clj-http "3.5.0"]
                 [fipp "0.6.8"]
                 [cheshire "5.6.1"]
                 [environ "1.1.0"]
                 [com.cemerick/url "0.1.1"]
                 [enlive "1.1.6"]
                 [bing-search "0.1.0"]
                 [enlive "1.1.1"]
                 [ring-cors "0.1.8"]
                 [metosin/compojure-api "1.1.1"]]
  :ring {:handler web-feed-search.handler/handler
         :nrepl {:start? false :port 9998}
         :adapter {:host ~(System/getenv "OPENSHIFT_DIY_IP")}}
  :uberjar-name "server.jar"
  :profiles {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]]
                   :plugins [[lein-ring "0.9.7"]]}})
